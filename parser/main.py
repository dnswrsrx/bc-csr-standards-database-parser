import openpyxl
import os
import sqlite3
import one.sheet_commands as sc
import one.database_commands as db


def main(csr, database_file):
    try:
        db.initialise_database(database_file)
        determine_parser_and_parse(csr, database_file)
        for output in test_sql_queries(database_file):
            print(tuple(output))
    finally:
        remove_database(database_file)


def determine_parser_and_parse(csr, database_file):
    """Iterates through sheet and determines which of two parser to use.

    Schedule 3.1 Part 1 uses one type of parser while the other schedules and
    parts of Schedule 3.1 use the other. Conveniently, the two 'types' of sheet
    can be determined by checking whether 'matrix' is in the string object of
    cell A1.
    """
    for sheet in csr:
        if "matrix" in sheet["A1"].value.lower():
            type_one_parse(sheet, database_file)
        else:
            type_two_parse(sheet, database_file)


def type_one_parse(sheet, database_file):
    db.populate_chemical_table(database_file, sc.chemical_arguments(sheet))
    db.populate_land_use_concentration_limit_table(
        database_file, sc.compile_land_use_concentration_limit_arguments(sheet)
    )


def type_two_parse(sheet, database_file):
    pass


def test_sql_queries(database_file):
    sql_query = """
        select * from land_use_concentration_limit
        where chemical="zinc";
    """
    with sqlite3.connect(database_file) as connection:
        connection.row_factory = sqlite3.Row
        cursor = connection.cursor()
        output = cursor.execute(sql_query)
    return output


def remove_database(database_file):
    os.remove(database_file)


def testing(csr, database_file):
    sheet = csr["Table 131"]
    return sheet["B4"].value
    # return sc.column_land_use_dict(sheet)


if __name__ == "__main__":
    csr = openpyxl.load_workbook(
        "/home/ds12/Dropbox/employment/current-islanderengineering/csr-33.xlsx"
    )
    base_directory = os.path.abspath(os.path.dirname(__file__))
    database_file = os.path.join(base_directory, "land_use_conc_limit.db")
    # main(csr, database_file)
    print(testing(csr, database_file))
