import openpyxl
import re
import one.cell_commands as cc


def chemical_arguments(sheet):
    """SQL query for populating chemical table.

    Will be run for every new sheet.
    """
    return (chemical_name(sheet), CAS(sheet), 3.1, matrix(sheet))


def chemical_name(sheet):
    return re.findall(
        r"standards[\d\s,]* ([\w\s\(\),\[\]]*) \(", sheet.cell(1, 1).value.lower()
    )[0]


def CAS(sheet):
    value = sheet.cell(1, 1).value
    if "not applicable" in value:
        return None
    else:
        return re.findall(r"\d+-\d+-\d+", value)[0]


def matrix(sheet):
    return int(re.findall(r"matrix (\d+)", sheet.cell(1, 1).value.lower())[0])


def compile_land_use_concentration_limit_arguments(sheet):
    return (
        cc.land_use_concentration_limit_arguments(
            sheet.cell(row, column).value,
            column_land_use_dict(sheet)[column],
            row_site_factor_dict(sheet)[row],
            chemical_name(sheet),
        )
        for row in row_site_factor_dict(sheet)
        for column in column_land_use_dict(sheet)
    )


LAND_USE_NAME_TO_CODE = {
    "wildlands natural": "wln",
    "wildlands reverted": "wlr",
    "agricultural": "al",
    "urban park": "pl",
    "residential low density": "rlld",
    "residential high density": "rlhd",
    "commercial": "cl",
    "industrial": "il",
    "aquatic life": "aw",
    "irrigation": "iw",
    "livestock": "lw",
    "drinking water": "dw",
    "parkade": "p",
}


def column_land_use_dict(sheet):
    output = {}
    for column in range(2, last_column(sheet) + 1):
        cell = sheet.cell(anchor_row(sheet), column)
        if isinstance(cell, openpyxl.cell.cell.Cell):
            output[column] = LAND_USE_NAME_TO_CODE[
                re.sub(r"\s*\(\w+\)", "", cell.value.lower())
            ]
            if "," in cell.value:
                output[column] = schedule_33_combined_land_use()
    return output


def schedule_33_combined_land_use():
    return [
        LAND_USE_NAME_TO_CODE["agricultural"],
        LAND_USE_NAME_TO_CODE["urban park"],
        LAND_USE_NAME_TO_CODE["residential low density"],
        LAND_USE_NAME_TO_CODE["residential high density"],
    ]


def row_site_factor_dict(sheet):
    output = {}
    history = None
    for row in range(anchor_row(sheet) + 1, last_row(sheet) + 1):
        cell = sheet.cell(row, 1).value.lower()
        next_cell = sheet.cell(row + 1, 1).value
        if "ph" in cell:
            output[row] = history.copy()
            output[row].update(ph_dict(cell))
        elif next_cell and any(
            map(lambda word: word in next_cell.lower(), ("freshwater", "ph"))
        ):
            history = site_specific_factor(cell)
        else:
            if site_specific_factor(cell):
                output[row] = site_specific_factor(cell)
    return output


DESCRIPTION_TO_SITE_FACTOR = {
    "intake of contaminated soil": "human_intake",
    "groundwater used for drinking": "human_drinking",
    "invertebrates and plants": "inverts_plants",
    "livestock ingesting soil": "livestock_ingestion",
    "microbial": "microbial",
    "groundwater flow": "groundwater_flow",
    "freshwater": "freshwater_flow",
    "marine": "marine_flow",
    "livestock watering": "watering",
    "irrigation": "irrigation",
}


def site_specific_factor(cell):
    for key in DESCRIPTION_TO_SITE_FACTOR:
        if key in cell:
            return {"site_factor": DESCRIPTION_TO_SITE_FACTOR[key]}


def ph_dict(cell_value):
    ph_output = [float(ph) for ph in re.findall(r"\d+.\d+", cell_value)]
    if "ph <" in cell_value:
        return {"ph_max": ph_output[0]}
    elif "<" in cell_value:
        return {"ph_min": ph_output[0], "ph_max": ph_output[1]}
    else:
        return {"ph_min": ph_output[0]}


# Following three methods help identify the range of cell table is in
def anchor_row(sheet):
    for row in range(1, sheet.max_row):
        if sheet.cell(row=row, column=1).value in ["Site-specific Factor", "Substance"]:
            return row


def last_row(sheet):
    for row in range(anchor_row(sheet), sheet.max_row + 1):
        if "irrigation" in sheet.cell(row=row, column=1).value:
            return last_ph_row_after_irrigation(sheet, row)


def last_ph_row_after_irrigation(sheet, row):
    next_row = row + 1
    next_cell = sheet.cell(row=next_row, column=1).value
    if next_cell and "ph" in next_cell.lower():
        return last_ph_row_after_irrigation(sheet, next_row)
    else:
        return row


def last_column(sheet):
    for column in range(1, sheet.max_column):
        cell = sheet.cell(row=anchor_row(sheet) - 1, column=column)
        if isinstance(cell, openpyxl.cell.cell.Cell):
            if cell.value.replace("\n", " ").lower() == "column 9":
                return column
