import unittest
import openpyxl


def different_coordinates(parsed, backup):
    output = []
    for sheet in backup.sheetnames:
        for coordinate in coordinates_to_loop(backup[sheet]):
            location = coordinate_if_unequal(parsed[sheet], backup[sheet], coordinate)
            if location:
                output.append((sheet, location))
    return output


def coordinates_to_loop(sheet):
    return (
        (row, column)
        for row in range(1, sheet.max_row + 1)
        for column in range(1, sheet.max_column + 1)
    )


def coordinate_if_unequal(sheet_parsed, sheet_backup, coordinate):
    row, column = coordinate
    parsed = sheet_parsed.cell(row, column)
    backup = sheet_parsed.cell(row, column)
    if isinstance(parsed, openpyxl.cell.cell.Cell) and isinstance(
        backup, openpyxl.cell.cell.Cell
    ):
        if sheet_parsed.cell(row, column).value != sheet_backup.cell(row, column).value:
            return coordinate


parsed_311 = openpyxl.load_workbook(
    "/home/ds12/Dropbox/employment/current-islanderengineering/csr-311.xlsx"
)
backup_311 = openpyxl.load_workbook(
    "/home/ds12/Dropbox/employment/current-islanderengineering/csr-311-backup.xlsx"
)
parsed_312 = openpyxl.load_workbook(
    "/home/ds12/Dropbox/employment/current-islanderengineering/csr-312.xlsx"
)
backup_312 = openpyxl.load_workbook(
    "/home/ds12/Dropbox/employment/current-islanderengineering/csr-312-backup.xlsx"
)
parsed_313 = openpyxl.load_workbook(
    "/home/ds12/Dropbox/employment/current-islanderengineering/csr-313.xlsx"
)
backup_313 = openpyxl.load_workbook(
    "/home/ds12/Dropbox/employment/current-islanderengineering/csr-313-backup.xlsx"
)
parsed_33 = openpyxl.load_workbook(
    "/home/ds12/Dropbox/employment/current-islanderengineering/csr-33.xlsx"
)
backup_33 = openpyxl.load_workbook(
    "/home/ds12/Dropbox/employment/current-islanderengineering/csr-33-backup.xlsx"
)


class TestCSRIntegrity(unittest.TestCase):
    def test_csr_311(self):
        self.assertEqual(different_coordinates(parsed_311, backup_311), [])

    def test_csr_312(self):
        self.assertEqual(different_coordinates(parsed_312, backup_312), [])

    def test_csr_313(self):
        self.assertEqual(different_coordinates(parsed_313, backup_313), [])

    def test_csr_33(self):
        self.assertEqual(different_coordinates(parsed_33, backup_33), [])


if __name__ == "__main__":
    unittest.main()
