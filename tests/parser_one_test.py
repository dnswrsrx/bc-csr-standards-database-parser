import unittest
import openpyxl
import sys
from os import path

parent_directory = path.dirname(path.dirname(path.abspath(__file__)))
# dirname gives the immediate directory that holds this "file"
sys.path.append(parent_directory)
sys.path.append("".join([parent_directory, "/parser"]))
# put parser within the path so can import directly from one


csr = openpyxl.load_workbook(
    "/home/ds12/Dropbox/employment/current-islanderengineering/csr-311.xlsx"
)


sheet_24 = csr["Table 24"]
sheet_25 = csr["Table 25"]
sheet_26 = csr["Table 26"]
# contains a cell with limit type min and different units
sheet_28 = csr["Table 28"]
# contains a merged cell
# contains parenthesis in name
sheet_32a = csr["Table 32a"]
# contains two words in name
sheet_34 = csr["Table 34"]
# contains pH subcategories
sheet_36 = csr["Table 36"]
# contains no CAS, output is None
sheet_52 = csr["Table 52"]
sheet_54 = csr["Table 54"]
# contains comma and square brackets in name


class TestSheetClass(unittest.TestCase):

    maxDiff = None

    def test_sheet_24_site_factor(self):
        self.assertEqual(sc.anchor_row(sheet_24), 3)

    def test_sheet_25_site_factor(self):
        self.assertEqual(sc.anchor_row(sheet_25), 3)

    def test_sheet_26_site_factor(self):
        self.assertEqual(sc.anchor_row(sheet_26), 3)

    def test_sheet_24_last_row(self):
        self.assertEqual(sc.last_row(sheet_24), 11)

    def test_sheet_25_last_row(self):
        self.assertEqual(sc.last_row(sheet_25), 13)

    def test_sheet_26_last_row(self):
        self.assertEqual(sc.last_row(sheet_26), 13)

    def test_sheet_34_last_row(self):
        self.assertEqual(sc.last_row(sheet_34), 43)

    def test_sheet_24_last_col(self):
        self.assertEqual(sc.last_column(sheet_24), 9)

    def test_sheet_25_last_col(self):
        self.assertEqual(sc.last_column(sheet_25), 9)

    def test_sheet_26_last_col(self):
        self.assertEqual(sc.last_column(sheet_26), 9)

    def test_sheet_28_last_col(self):
        self.assertEqual(sc.last_column(sheet_28), 10)

    def test_sheet_24_chemical_name(self):
        self.assertEqual(sc.chemical_name(sheet_24), "anthracene")

    def test_sheet_25_chemical_name(self):
        self.assertEqual(sc.chemical_name(sheet_25), "arsenic")

    def test_sheet_26_chemical_name(self):
        self.assertEqual(sc.chemical_name(sheet_26), "barium")

    def test_sheet_28_chemical_name(self):
        self.assertEqual(sc.chemical_name(sheet_28), "benzo(a)pyrene")

    def test_sheet_32a_chemical_name(self):
        self.assertEqual(sc.chemical_name(sheet_32a), "chromium hexavalent")

    def test_sheet_54_chemical_name(self):
        self.assertEqual(
            sc.chemical_name(sheet_54), "polychlorinated biphenyls, total [pcbs]"
        )

    def test_sheet_24_CAS(self):
        self.assertEqual(sc.CAS(sheet_24), "120-12-7")

    def test_sheet_25_CAS(self):
        self.assertEqual(sc.CAS(sheet_25), "7440-38-2")

    def test_sheet_36_CAS(self):
        self.assertEqual(sc.CAS(sheet_36), None)

    def test_sheet_24_matrix(self):
        self.assertEqual(sc.matrix(sheet_24), 1)

    def test_sheet_25_matrix(self):
        self.assertEqual(sc.matrix(sheet_25), 2)

    def test_sheet_24_land_use_column(self):
        self.assertEqual(
            sc.column_land_use_dict(sheet_24),
            {
                2: "wln",
                3: "wlr",
                4: "al",
                5: "pl",
                6: "rlld",
                7: "rlhd",
                8: "cl",
                9: "il",
            },
        )

    def test_sheet_25_land_use_column(self):
        self.assertEqual(
            sc.column_land_use_dict(sheet_25),
            {
                2: "wln",
                3: "wlr",
                4: "al",
                5: "pl",
                6: "rlld",
                7: "rlhd",
                8: "cl",
                9: "il",
            },
        )

    def test_sheet_28_land_use_column(self):
        self.assertEqual(
            sc.column_land_use_dict(sheet_28),
            {
                2: "wln",
                3: "wlr",
                4: "al",
                6: "pl",
                7: "rlld",
                8: "rlhd",
                9: "cl",
                10: "il",
            },
        )

    def test_sheet_24_row_column(self):
        self.assertEqual(
            sc.row_site_factor_dict(sheet_24),
            {
                4: {"site_factor": "human_intake"},
                5: {"site_factor": "human_drinking"},
                6: {"site_factor": "inverts_plants"},
                7: {"site_factor": "livestock_ingestion"},
                8: {"site_factor": "microbial"},
                9: {"site_factor": "groundwater_flow"},
                10: {"site_factor": "watering"},
                11: {"site_factor": "irrigation"},
            },
        )

    def test_sheet_25_row_column(self):
        self.assertEqual(
            sc.row_site_factor_dict(sheet_25),
            {
                4: {"site_factor": "human_intake"},
                5: {"site_factor": "human_drinking"},
                6: {"site_factor": "inverts_plants"},
                7: {"site_factor": "livestock_ingestion"},
                8: {"site_factor": "microbial"},
                10: {"site_factor": "freshwater_flow"},
                11: {"site_factor": "marine_flow"},
                12: {"site_factor": "watering"},
                13: {"site_factor": "irrigation"},
            },
        )

    def test_sheet_34_row_column(self):
        self.assertEqual(
            sc.row_site_factor_dict(sheet_34),
            {
                4: {"site_factor": "human_intake"},
                6: {"site_factor": "human_drinking", "ph_max": 5.0},
                7: {"site_factor": "human_drinking", "ph_min": 5.0, "ph_max": 5.5},
                8: {"site_factor": "human_drinking", "ph_min": 5.5, "ph_max": 6.0},
                9: {"site_factor": "human_drinking", "ph_min": 6.0, "ph_max": 6.5},
                10: {"site_factor": "human_drinking", "ph_min": 6.5, "ph_max": 7.0},
                11: {"site_factor": "human_drinking", "ph_min": 7.0},
                14: {"site_factor": "inverts_plants"},
                15: {"site_factor": "livestock_ingestion"},
                16: {"site_factor": "microbial"},
                19: {"site_factor": "freshwater_flow", "ph_max": 5.5},
                20: {"site_factor": "freshwater_flow", "ph_min": 5.5, "ph_max": 6.0},
                21: {"site_factor": "freshwater_flow", "ph_min": 6.0, "ph_max": 6.5},
                22: {"site_factor": "freshwater_flow", "ph_min": 6.5, "ph_max": 7.0},
                23: {"site_factor": "freshwater_flow", "ph_min": 7.0, "ph_max": 7.5},
                24: {"site_factor": "freshwater_flow", "ph_min": 7.5},
                26: {"site_factor": "marine_flow", "ph_max": 6.0},
                27: {"site_factor": "marine_flow", "ph_min": 6.0, "ph_max": 6.5},
                28: {"site_factor": "marine_flow", "ph_min": 6.5, "ph_max": 7.0},
                29: {"site_factor": "marine_flow", "ph_min": 7.0},
                31: {"site_factor": "watering", "ph_max": 5.0},
                32: {"site_factor": "watering", "ph_min": 5.0, "ph_max": 5.5},
                33: {"site_factor": "watering", "ph_min": 5.5, "ph_max": 6.0},
                34: {"site_factor": "watering", "ph_min": 6.0, "ph_max": 6.5},
                35: {"site_factor": "watering", "ph_min": 6.5, "ph_max": 7.0},
                36: {"site_factor": "watering", "ph_min": 7.0, "ph_max": 7.5},
                37: {"site_factor": "watering", "ph_min": 7.5},
                39: {"site_factor": "irrigation", "ph_max": 5.5},
                40: {"site_factor": "irrigation", "ph_min": 5.5, "ph_max": 6.0},
                41: {"site_factor": "irrigation", "ph_min": 6.0, "ph_max": 6.5},
                42: {"site_factor": "irrigation", "ph_min": 6.5, "ph_max": 7.0},
                43: {"site_factor": "irrigation", "ph_min": 7.0},
            },
        )

    def test_sheet_24_sql_query_chemical_table(self):
        self.assertEqual(
            sc.chemical_arguments(sheet_24), ("anthracene", "120-12-7", 3.1, 1)
        )

    def test_sheet_28_sql_query_chemical_table(self):
        self.assertEqual(
            sc.chemical_arguments(sheet_28), ("benzo(a)pyrene", "50-32-8", 3.1, 5)
        )

    def test_sheet_32a_sql_query_chemical_table(self):
        self.assertEqual(
            sc.chemical_arguments(sheet_32a),
            ("chromium hexavalent", "7440-47-3", 3.1, 9),
        )

    def test_sheet_54_sql_query_chemical_table(self):
        self.assertEqual(
            sc.chemical_arguments(sheet_54),
            ("polychlorinated biphenyls, total [pcbs]", "1336-36-3", 3.1, 29),
        )

    def test_sheet_36_sql_query_chemical_table(self):
        self.assertEqual(
            sc.chemical_arguments(sheet_36),
            ("dichlorodiphenyltrichloroethane, total [ddt]", None, 3.1, 13),
        )


class TestCellClass(unittest.TestCase):
    def test_spaced_numbers_to_value(self):
        self.assertEqual(cc.value(sheet_24["h4"].value), 75000.0)

    def test_24_f6_str(self):
        self.assertIsInstance(sheet_24["f6"].value, str)

    def test_str_to_value(self):
        self.assertEqual(cc.value(sheet_24["f6"].value), 2.5)

    def test_24_g6_int(self):
        self.assertIsInstance(sheet_24["g6"].value, int)

    def test_int_to_value(self):
        self.assertEqual(cc.value(sheet_24["g6"].value), 30)

    def test_52_d18_float(self):
        self.assertIsInstance(sheet_52["d18"].value, float)

    def test_float_to_value(self):
        self.assertEqual(cc.value(sheet_52["d18"].value), 0.35)

    def test_unit_and_limit_to_value(self):
        self.assertEqual(cc.value(sheet_24["i4"].value), 1000)

    def test_max_limit_type(self):
        self.assertEqual(cc.limit_type(sheet_24["h4"].value), "max")

    def test_min_limit_type(self):
        self.assertEqual(cc.limit_type(sheet_24["i4"].value), "min")

    def test_regular_unit(self):
        self.assertEqual(cc.unit(sheet_24["h4"].value), "ug/g")

    def test_specified_unit(self):
        self.assertEqual(cc.unit(sheet_24["i4"].value), "mg/g")

    def test_NS_value(self):
        self.assertEqual(cc.value(sheet_24["d7"].value), None)

    def test_NS_unit(self):
        self.assertEqual(cc.unit(sheet_24["d7"].value), None)

    def test_NS_limit_type(self):
        self.assertEqual(cc.limit_type(sheet_24["d7"].value), None)

    def test_blank_value(self):
        self.assertEqual(cc.value(sheet_24["e7"].value), None)

    def test_blank_unit(self):
        self.assertEqual(cc.unit(sheet_24["e7"].value), None)

    def test_blank_limit_type(self):
        self.assertEqual(cc.limit_type(sheet_24["e7"].value), None)


if __name__ == "__main__":
    import one.sheet_commands as sc
    import one.cell_commands as cc

    unittest.main()
